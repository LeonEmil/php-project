<?php

    include('settings/db_connect.php');

    $errors = array('name' => '', 'email' => '', 'ingredients'=> '');

    if(isset($_POST['submit'])){
        if(empty($_POST['name'])){
            $errors['name'] = 'Escriba un nombre';
        }
        if(empty($_POST['email'])){
            $errors['email'] = 'Escriba un email';
        }
        if(empty($_POST['ingredients'])){
            $errors['ingredients'] = 'Escriba un ingrediente';
        }
        if(array_filter($errors)){
            echo "Hay error";
        }
        else {
            $name = mysqli_real_escape_string($connect, $_POST['name']);
            $ingredients = mysqli_real_escape_string($connect, $_POST['ingredients']);
            $sendDataQuery = "INSERT INTO pizzas(name, ingredients) VALUES ('$name', '$ingredients')";
    
            if(mysqli_query($connect, $sendDataQuery)){
                header('Location: index.php');
            }
        }
    } else {
        $_POST['name'] = $_POST['email'] = $_POST['ingredients'] = '';
    }

   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php include('components/header.php'); ?> 

    <form action="contact.php" method="POST" class="form">
        <label for="name"></label>
        <input type="text "id="name" name="name" value= <?php echo $_POST['name'];?>>
        <p><?php echo($errors['name']);?></p>

        <label for="email"></label>
        <input type="email "id="email" name="email" value= <?php echo $_POST['email'];?>>
        <p><?php echo($errors['email']);?></p>

        <label for="ingredients"></label>
        <input type="text "id="ingredients" name="ingredients" value= <?php echo $_POST['ingredients'];?>>
        <p><?php echo($errors['ingredients']);?></p>

        <input type="submit" name="submit" value="Enviar">
    </form>
    
    <?php include('components/footer.php'); ?> 
</body>
</html>
