<main class="main">
    <h2 class="main__title">Content</h2>

    <?php foreach($response as $pizza){ ?>
        <div>
            <h3><?php echo htmlspecialchars($pizza['name']) ?></h3>
            <ul>
                <?php foreach(explode(',', $pizza['ingredients']) as $ingredient){ ?>
                    <li><?php echo $ingredient ?></li>
                <?php } ?> 
            </ul>
            <a href="details.php?id=<?php echo $pizza['id'] ?>">See details</a>
        </div>
    <?php } ?>
</main>